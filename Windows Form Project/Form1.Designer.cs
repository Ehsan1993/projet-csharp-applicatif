﻿namespace Windows_Form_Project
{
    partial class ConnexionSQLServer
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.name = new System.Windows.Forms.Label();
            this.lname = new System.Windows.Forms.Label();
            this.city = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.textBox_lname = new System.Windows.Forms.TextBox();
            this.textBox_city = new System.Windows.Forms.TextBox();
            this.insert = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.adresse = new System.Windows.Forms.Label();
            this.instrument = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.Label();
            this.mail = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.textBox_code = new System.Windows.Forms.TextBox();
            this.textBox_adresse = new System.Windows.Forms.TextBox();
            this.textBox_mail = new System.Windows.Forms.TextBox();
            this.textBox_phone = new System.Windows.Forms.TextBox();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.textBox_instrument = new System.Windows.Forms.TextBox();
            this.title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(125, 98);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(71, 24);
            this.name.TabIndex = 1;
            this.name.Text = "Name";
            // 
            // lname
            // 
            this.lname.AutoSize = true;
            this.lname.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lname.Location = new System.Drawing.Point(425, 98);
            this.lname.Name = "lname";
            this.lname.Size = new System.Drawing.Size(96, 24);
            this.lname.TabIndex = 2;
            this.lname.Text = "L_Name";
            this.lname.Click += new System.EventHandler(this.label3_Click);
            // 
            // city
            // 
            this.city.AutoSize = true;
            this.city.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.city.Location = new System.Drawing.Point(139, 147);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(57, 24);
            this.city.TabIndex = 3;
            this.city.Text = "City";
            this.city.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox_name
            // 
            this.textBox_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_name.Location = new System.Drawing.Point(210, 103);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(185, 19);
            this.textBox_name.TabIndex = 4;
            this.textBox_name.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox_lname
            // 
            this.textBox_lname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_lname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_lname.Location = new System.Drawing.Point(527, 103);
            this.textBox_lname.Name = "textBox_lname";
            this.textBox_lname.Size = new System.Drawing.Size(185, 19);
            this.textBox_lname.TabIndex = 4;
            // 
            // textBox_city
            // 
            this.textBox_city.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_city.Location = new System.Drawing.Point(210, 152);
            this.textBox_city.Name = "textBox_city";
            this.textBox_city.Size = new System.Drawing.Size(185, 19);
            this.textBox_city.TabIndex = 4;
            // 
            // insert
            // 
            this.insert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.insert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insert.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insert.Location = new System.Drawing.Point(210, 386);
            this.insert.Name = "insert";
            this.insert.Size = new System.Drawing.Size(100, 35);
            this.insert.TabIndex = 5;
            this.insert.Text = "Insert";
            this.insert.UseVisualStyleBackColor = false;
            this.insert.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(457, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Code";
            this.label1.Click += new System.EventHandler(this.label4_Click);
            // 
            // adresse
            // 
            this.adresse.AutoSize = true;
            this.adresse.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adresse.Location = new System.Drawing.Point(102, 194);
            this.adresse.Name = "adresse";
            this.adresse.Size = new System.Drawing.Size(94, 24);
            this.adresse.TabIndex = 3;
            this.adresse.Text = "Adresse";
            this.adresse.Click += new System.EventHandler(this.label4_Click);
            // 
            // instrument
            // 
            this.instrument.AutoSize = true;
            this.instrument.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrument.Location = new System.Drawing.Point(62, 284);
            this.instrument.Name = "instrument";
            this.instrument.Size = new System.Drawing.Size(134, 24);
            this.instrument.TabIndex = 3;
            this.instrument.Text = "Instrument";
            this.instrument.Click += new System.EventHandler(this.label4_Click);
            // 
            // phone
            // 
            this.phone.AutoSize = true;
            this.phone.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone.Location = new System.Drawing.Point(444, 241);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(77, 24);
            this.phone.TabIndex = 3;
            this.phone.Text = "Phone";
            this.phone.Click += new System.EventHandler(this.label4_Click);
            // 
            // mail
            // 
            this.mail.AutoSize = true;
            this.mail.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail.Location = new System.Drawing.Point(115, 239);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(81, 24);
            this.mail.TabIndex = 3;
            this.mail.Text = "E-mail";
            this.mail.Click += new System.EventHandler(this.label4_Click);
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Bookman Old Style", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(86, 328);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(110, 24);
            this.password.TabIndex = 3;
            this.password.Text = "Password";
            this.password.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox_code
            // 
            this.textBox_code.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_code.Location = new System.Drawing.Point(527, 152);
            this.textBox_code.Name = "textBox_code";
            this.textBox_code.Size = new System.Drawing.Size(185, 19);
            this.textBox_code.TabIndex = 4;
            // 
            // textBox_adresse
            // 
            this.textBox_adresse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_adresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_adresse.Location = new System.Drawing.Point(210, 197);
            this.textBox_adresse.Name = "textBox_adresse";
            this.textBox_adresse.Size = new System.Drawing.Size(502, 19);
            this.textBox_adresse.TabIndex = 4;
            // 
            // textBox_mail
            // 
            this.textBox_mail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_mail.Location = new System.Drawing.Point(210, 244);
            this.textBox_mail.Name = "textBox_mail";
            this.textBox_mail.Size = new System.Drawing.Size(185, 19);
            this.textBox_mail.TabIndex = 4;
            // 
            // textBox_phone
            // 
            this.textBox_phone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_phone.Location = new System.Drawing.Point(527, 246);
            this.textBox_phone.Name = "textBox_phone";
            this.textBox_phone.Size = new System.Drawing.Size(185, 19);
            this.textBox_phone.TabIndex = 4;
            // 
            // textBox_password
            // 
            this.textBox_password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_password.Location = new System.Drawing.Point(210, 331);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.PasswordChar = '*';
            this.textBox_password.Size = new System.Drawing.Size(185, 23);
            this.textBox_password.TabIndex = 4;
            // 
            // textBox_instrument
            // 
            this.textBox_instrument.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_instrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_instrument.Location = new System.Drawing.Point(210, 289);
            this.textBox_instrument.Name = "textBox_instrument";
            this.textBox_instrument.Size = new System.Drawing.Size(502, 19);
            this.textBox_instrument.TabIndex = 4;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Bookman Old Style", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(105, 21);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(330, 41);
            this.title.TabIndex = 6;
            this.title.Text = "Inscription Form";
            // 
            // ConnexionSQLServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cyan;
            this.ClientSize = new System.Drawing.Size(809, 457);
            this.Controls.Add(this.title);
            this.Controls.Add(this.insert);
            this.Controls.Add(this.textBox_instrument);
            this.Controls.Add(this.textBox_password);
            this.Controls.Add(this.textBox_phone);
            this.Controls.Add(this.textBox_mail);
            this.Controls.Add(this.textBox_adresse);
            this.Controls.Add(this.textBox_code);
            this.Controls.Add(this.textBox_city);
            this.Controls.Add(this.textBox_lname);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.password);
            this.Controls.Add(this.mail);
            this.Controls.Add(this.phone);
            this.Controls.Add(this.instrument);
            this.Controls.Add(this.adresse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.city);
            this.Controls.Add(this.lname);
            this.Controls.Add(this.name);
            this.Name = "ConnexionSQLServer";
            this.Text = "Connexion SQL Server";
            this.Load += new System.EventHandler(this.ConnexionSQLServer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label lname;
        private System.Windows.Forms.Label city;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.TextBox textBox_lname;
        private System.Windows.Forms.TextBox textBox_city;
        private System.Windows.Forms.Button insert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label adresse;
        private System.Windows.Forms.Label instrument;
        private System.Windows.Forms.Label phone;
        private System.Windows.Forms.Label mail;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox textBox_code;
        private System.Windows.Forms.TextBox textBox_adresse;
        private System.Windows.Forms.TextBox textBox_mail;
        private System.Windows.Forms.TextBox textBox_phone;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.TextBox textBox_instrument;
        private System.Windows.Forms.Label title;
    }
}

